(defproject batless-barry-bonds "0.1.0-SNAPSHOT"
  :description "Simulate Barry Bonds' 2004 season, but without a bat."
  :url "https://gitlab.com/jasonmm/batless-barry-bonds"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/data.csv "0.1.4"]
                 [clojure.java-time "0.3.2"]]
  :main ^:skip-aot batless-barry-bonds.core)
