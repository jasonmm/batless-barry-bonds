# Description

Recreate the simulation described in [this video](https://www.youtube.com/watch?v=JwMfT2cZGHg).  Note: [this comment](https://www.youtube.com/watch?v=JwMfT2cZGHg&lc=Ugyam6zIm4Q4xnzKDyZ4AaABAg) makes an important point.

    2018-06-29: development has been paused due to the number of plate appearances in the data appearing to be greater than Bonds' actual number of plate appearances in 2004. Without going through each plate appearance in the data one by one and matching it up with the [baseball-reference boxscores](https://www.baseball-reference.com/teams/SFG/2004-schedule-scores.shtml), I'm not sure of how to solve that.

# First Attempt

The Retrosheet files have several types of lines in them.  The ones we are interested in start with the word "play"). At first it appears that those lines contain exactly one play each. This means it seemed plausible to just get the records that list Bonds as the player and only consider those records. However, on further reading, some lines are continuations of previous lines.

This first attempt at solving the problem took each "play" line for a player as a single plate appearance unless it started with a period. "play" lines that started with a period were appended to the previous "play" line for that player. This resulted in two more plate appearances being found than Bonds had in 2004. Finding those two extra plate appearances was difficult.

While looking for the two extra plate appearances it was discovered that the Retrosheet files seemed to contain errors. In the file `2004SFN.EVN` the following lines appeared:

    play,4,1,bondb001,00,,NP
    sub,alfoa001,"Antonio Alfonseca",0,6,1
    play,4,1,bondb001,00,,NP
    sub,hessm001,"Mike Hessman",0,9,3
    play,4,1,bondb001,01,..C>B,SB3
    play,4,1,bondb001,31,..C>B.III,IW.1-2

This results in the program combining these lines into the pitch sequence "CBCBIII". Which would given Bonds 5 balls during that plate appearance. Instead the last line's pitch sequence should read ".III". This would show that during Bonds' plate appearance there were two "no play" events (for substitutions) then Bonds received a called strike, a ball, somone stole 3rd base, and finally Bonds received three straight intentional balls. This does not explain the two extra plate appearances found, however.

Also consider the following sequence of records:

    play,11,1,walkt003,00,,NP
    sub,nunea002,"Abraham Nunez",0,6,7
    play,11,1,walkt003,00,,NP
    sub,perim001,"Matt Perisho",0,7,1
    play,11,1,walkt003,00,,NP
    sub,bondb001,"Barry Bonds",1,5,11
    play,11,1,bondb001,32,...BFFBBB,W

Here the defense makes two substitutions and the offense brings in Barry Bonds to pinch hit. However, because Bonds' record starts with three periods, and the program is only considering records with Bonds, that final line is combined with Bonds' last plate appearance in the previous game. Instead, the program should recognize that Bonds was a pinch hitter and not combine the last line with Bonds' previous at-bat. This results in increasing Bonds' plate appearance count, which the program is already placing too high. However, it demonstrated that a different approach to reading the Retrosheet files was needed.

# Second Attempt

Because of the above discoveries the next approach was to read all "play" lines in the Retrosheet files and combine all events for a single play into one vector. Then if the play included Bonds at all it would be kept and the pitches given to Bonds would be extracted.

So for the last example above, the play vector would look something like this:

    [
      ["walkt003" ""]
      ["walkt003" ""]
      ["walkt003" ""]
      ["bondb001" "...BFFBBB"]
    ]

This shows that for the first 3 events in the "play", that _walkt003_ received zero pitches. Then _bondb001_ received 6 pitches.  We can ignore the periods and the three _walkt003_ events and just keep the pitches thrown to Bonds. It also shows that the original reading of the Retrosheet rules for continuation lines may have been incorrect.  Rather than a continuation line showing only the pitches that were thrown after the non-batter event (e.g. a stolen base) they contain all the pitches with a period to hold the place of the non-batter event.  Most periods represent substitutions (i.e. "NP" lines) and so there are rarely, if ever, any pitches prior to the substitution.

Following this thinking the program would see the three periods in the last vector and know it can discard the three immediately previous event vectors, then remove non-pitch characters leaving the play vector to be:

    [["bondb001" "BFFBBB"]]

Consider these two lines:

    play,5,1,bondb001,12,BC+2F>B,SB3
    play,5,1,bondb001,32,BC+2F>B.II,IW.1-2

This shows that Bonds the sequence of events to be: ball, called strike, pick-off throw to 2nd base, foul ball, a runner advance on a ball (the steal of 3rd base), and then two intentional balls.  In this case the program should see periods and throw away that number of previous events in the play.  So the play vector would be:

    [
      ["bondb001" "BC+2F>B"]
      ["bondb001" "BC+2F>B.II"]
    ]

Because there is one period in the last event we can throw away the immediately previous event vector, then remove non-pitch characters leaving the play vector to be:

    [["bondb001" "BCFBII"]]

So the first example in the **First Attempt** section above is not a mistake and the play vector would be:

    [
      ["bondb001" ""]
      ["bondb001" ""]
      ["bondb001" "..C>B"]
      ["bondb001" "..C>B.III"]
    ]

Which would become:

    [["bondb001" "..C>B.III"]]

And finally become:

    [["bondb001" "CBIII"]]

The program cannot just view Bonds' lines in isolation because it seems possible to have the following:

    [
      ["bondb001" "BC"]
      ["tuckm001" ".CS"]
    ]

This fictitious play vector means that Bonds was removed in the middle of the at-bat and _tuckm001_ was brought in to finish the at-bat. Looking just at lines for Bonds there is no way to know the plate appearance continued with the a different player. Additionally, the rules for mid-at-bat substitutions [are complicated](https://sports.stackexchange.com/a/901). So for the purpose of this program we are only going to count at-bats that were finished by Bonds.  This is likely a safe approach since Bonds does not appear to have been removed mid-at-bat during the 2004 season.

### Algorithm

1. For each event file:
    1. Add the game ID to each line's vector.
    1. Remove all lines that do not start with "play".
    1. Partition the lines into groups splitting on game ID (i.e. the last item in each line's vector).
    1. For each partition (i.e. game):
        1. Using `reduce` create the play vectors by adding lines with a period to the previous play vector.
        1. For each play vector:
            1. Ignore if the final event vector is not for "bondb001".
            1. Ensure the final event vector's pitch sequence has either zero or `(- (count play-vector) 1)` periods.
            1. Drop the first `num-periods` event vectors from the play vector.
            1. Remove non-pitch characters from the pitch sequence.
            1. Map to the pitch sequence.
    1. Combine the paritions (which should now be sequences of strings).
1. Combine the event file data (which should now be sequences of strings).

##### New attempt at algorithm to help figure out PA discrepency.

1. For each event file:
    1. Create [{:game-id "..." :home "BAL" :away "BOS" :date #inst :plays [play-map play-map play-map]}
               {:game-id "..." :home "BAL" :away "tor" :date #inst :plays [play-map play-map play-map]}]
    1. For each game
        1. Remove non-Bonds plays from :plays.
        1. Remove play maps that do not have a batter outcome.
1. Combine the maps created from all the event files.
1. Sort on :date

The result should be a sequence of strings showing the pitches for every one of Bonds' plate appearances in 2004.

##### Problem event

There is one event in the 2004BAL.EVA file where Bonds' AB follows a comment.  This event starts with a period which, means it needs to replace the previous event.  However, the previous Bonds event is not related so it gets overwritten and lost. This results in the loss of one plate appearance for Bonds.

##### Running the simulation

Simulating the batless season 500, 5,000, and 500,000 times (the 500,000 one takes about an hour to run):

```
batless-barry-bonds.core> (time (run-times "../2004eve" "bondb001" 500))
"Elapsed time: 6344.939841 msecs"
{:max 0.623, :min 0.532, :median [0.576 0.576], :mode 0.583}
batless-barry-bonds.core> (time (run-times "../2004eve" "bondb001" 5000))
"Elapsed time: 37860.375229 msecs"
{:max 0.633, :min 0.524, :median [0.576 0.576], :mode 0.576}
batless-barry-bonds.core> (time (run-times "../2004eve" "bondb001" 500000))
"Elapsed time: 3503198.881211 msecs"
{:max 0.644, :min 0.515, :median [0.576 0.576], :mode 0.576}
```

Note: the above percentages do not include hit-by-pitch when calculating on-base percentage (which is incorrect).

Here are the same simulations with hit-by-pitch included in the on-base percentage calculation:

```
batless-barry-bonds.core> (time (run-times "../2004eve" "bondb001" 500))
"Elapsed time: 6252.012525 msecs"
{:max 0.631, :min 0.552, :median [0.589 0.589], :mode 0.584}
batless-barry-bonds.core> (time (run-times "../2004eve" "bondb001" 5000))
"Elapsed time: 37497.056713 msecs"
{:max 0.636, :min 0.541, :median [0.591 0.591], :mode 0.591}
batless-barry-bonds.core> (time (run-times "../2004eve" "bondb001" 500000))
"Elapsed time: 3428248.483833 msecs"
{:max 0.662, :min 0.529, :median [0.591 0.591], :mode 0.591}
```
