(ns batless-barry-bonds.core
  (:require [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [java-time :as jtime]))


;;; We get all the percentages from the video
;;; (https://www.youtube.com/watch?v=JwMfT2cZGHg).

;; 80.9% of the time Bonds swung the pitch was in the strikezone.
(def strikezone-pct-when-swinging 809)
;; 41.3% of the all pitches Bonds saw were in the strikezone.
(def strikezone-pct-all-pitches 413)

(def non-pitch-chars
  "A set of characters that we want removed from the pitch strings."
  #{\* \. \> \+ \1 \2 \3 \4})

(def pitch-chars
  "A set of characters that can be used to represent a pitch.
  See http://www.retrosheet.org/eventfile.htm#3"
  #{\B \C \F \H \I \K \L \M \O \P \Q \R \S \T \V \X \Y})

(defn remove-non-pitch-chars
  [s]
  (->> s
       (remove #(non-pitch-chars %))
       str/join))

(defn swinging-result
  "Determine the result of a batless pitch if the real pitch was swung at."
  []
  (let [rand-num (inc (rand-int 1000))
        k?       (> strikezone-pct-when-swinging rand-num)]
    (if k? \K \B)))

(defn generic-result
  "Determine the result of a batless pitch if we do not know whether the real
  pitch was swung at or not."
  []
  (let [rand-num (inc (rand-int 1000))
        k?       (> strikezone-pct-all-pitches rand-num)]
    (if k? \K \B)))

(defn convert-pitch
  "Return either H, B or K depending on the real-life pitch.

  H means hit-by-pitch.

  B means ball.

  We use K for all strikes as our batless player can only have one kind of
  strike and K is the universal symbol for strike/strikeout.

  See https://www.retrosheet.org/eventfile.htm#3 for a description of what the
  letters mean in a retrosheet file."
  [pitch-char]
  (case pitch-char
    \B \B
    \C \K
    \F (swinging-result)
    \H \H
    \I \B
    \K \K
    \L (swinging-result)
    \M (swinging-result)
    \O (swinging-result)
    \P \B
    \Q \K
    \R \B
    \S (swinging-result)
    \T (swinging-result)
    \V \B
    \X (swinging-result)
    \Y \B
    \?))

(defn event-file?
  "Whether or not the given `file` is an event file."
  [file]
  (let [ext (str/join (take 3 (reverse (.getName file))))]
    (or (= "NVE" ext) (= "AVE" ext))))

(defn retrosheet-event-files
  "Returns a sequence of files in `dir` that are retrosheet event files."
  [dir]
  (->> dir
       io/file
       file-seq
       (filter event-file?)))

(defn pad-batless-pitches
  "Given a sequence of pitch chars make sure there are at least 6 pitches in
  that sequence. This is because a batless at-bat can only have at most 6
  pitches."
  [pitch-seq]
  (take 6 (concat pitch-seq (repeatedly generic-result))))

(defn add-batless-pitch-str
  "Add a key called `:converted` that contains the pitch string for the
  batless batter."
  [pitch-map]
  (merge pitch-map
         {:converted (->> pitch-map
                          :original
                          (map convert-pitch)
                          pad-batless-pitches
                          str/join)}))

(defn game-seq->game-id
  "Given a sequence of row vectors representing a game return the game's ID."
  [game-seq]
  (->> game-seq
       (filter #(= "id" (first %)))
       first
       second))

(defn game-seq->info-value
  "Returns the value of the 'info' item specified by `info-name`.
  Example: `(game-seq->info-value game-seq \"hometeam\")` will return the home
  team."
  [game-seq info-name]
  (let [info-row (->> game-seq
                      (filter #(= info-name (second %)))
                      first)]
    (nth info-row 2)))

(defn play-vector->play-map
  "Convert a raw vector as read from a CSV file for a 'play' row into a map
  with descriptive keys."
  [play-vector]
  (let [play-map-keys [:inning :team :player-id :count :pitches :outcome]
        play-map      (zipmap play-map-keys (rest play-vector))]
    (assoc play-map
           :inning (Integer/parseInt (:inning play-map))
           :team (if (= "0" (:team play-map)) :away :home))))

(defn game-seq->plays
  "Returns play maps for all the 'play' rows in the `game-seq`."
  [game-seq]
  (->> game-seq
       (filter #(= "play" (first %)))
       (map play-vector->play-map)))

(defn game-seq->game-map
  [game-seq]
  (let [game-id    (game-seq->game-id game-seq)
        hometeam   (game-seq->info-value game-seq "hometeam")
        awayteam   (game-seq->info-value game-seq "visteam")
        dateplayed (jtime/local-date "yyyy/MM/dd" (game-seq->info-value game-seq "date"))
        plays      (game-seq->plays game-seq)]
    {:game-id game-id
     :home    hometeam
     :away    awayteam
     :date    dateplayed
     :plays   plays}))

(defn player-play?
  "Whether or not the `play-map` is a play for `player-id`."
  [play-map player-id]
  (= player-id (:player-id play-map)))

(defn remove-plays
  "Removes plays from the `game-map`'s `:plays` collection that do not involve
  `player-id`."
  [game-map player-id]
  (->> game-map
       :plays
       (filter #(player-play? % player-id))
       (assoc game-map :plays)))

(defn game-has-plays?
  "Returns true if there are plays in the `game-map`."
  [game-map]
  (seq (:plays game-map)))

(defn play-map-reducer
  "Used in a call to `reduce`, this function removes play maps from `carry`
  that are followed by play maps containing periods.  Essentially removing play
  maps that do not have a batter outcome. `carry` is a vector that will become
  the collection of play maps for a game after the `reduce` has finished."
  [carry play-map]
  (let [num-periods (get (frequencies (:pitches play-map)) \.)]
    (if num-periods
      (conj (vec (drop-last num-periods carry)) play-map)
      (conj carry play-map))))

(defn remove-extra-play-maps
  "Removes play maps that do not have a batter outcome."
  [game-map]
  (->> game-map
       :plays
       (reduce play-map-reducer [])
       (sort-by :inning)
       (assoc game-map :plays)))

(defn file->gamemap
  "Given an event `file` and a `player-id` return a sequence of game maps
  containing plays involving that player."
  [file player-id]
  (with-open [reader (io/reader file)]
    (let [data (csv/read-csv reader)]
      (->> data
           ;; We want to group the vectors as parsed from the CSV into groups
           ;; by game, but `partition-by` separates the "id" row for the game
           ;; from the rest of the game's rows...
           (partition-by #(= "id" (first %)))
           ;; ...so we combine the ID row into a seq with the rest of the game's
           ;; rows, meaning we now have a collection with two sequences inside it.
           ;; The first sequence contains the ID row and the second sequence
           ;; contains all the other rows for the game...
           (partition 2)
           ;; ...finally we put that ID row at the top of a seq of its associated rows
           (map (fn [s] (let [id-vec (ffirst s)] (conj (second s) id-vec))))
           ;; ...now we have a sequence of collections of vectors where each
           ;; collection represents one game, and the vectors inside each
           ;; collection are the parsed lines from the CSV file for that game.

           ;; convert each collection of vectors describing a game into a map
           (map game-seq->game-map)

           ;; remove plays not involving the player
           (map #(remove-plays % player-id))

           ;; remove previous x play-maps if a play-map has periods in the pitch sequence.
           (map remove-extra-play-maps)

           ;; remove games that no longer have plays; i.e. `player-id` did not
           ;; play in that game.
           (filter game-has-plays?)

           ;; prevent laziness
           vec

           #_(clojure.pprint/pprint)))))

(defn add-batless-outcome
  "Given a map containing keys `:original` and `:converted` add the key
  `:outcome` containing either a K, B, or H to denote the outcome of the
  `:converted` sequence of pitches."
  [pitch-map]
  (assoc pitch-map :outcome
         (reduce (fn [carry pitch]
                   (let [carry (case pitch
                                 \H (reduced \H)
                                 \K (update carry :k inc)
                                 \B (update carry :b inc))]
                     (if (= 4 (:b carry))
                       (reduced \B)
                       (if (= 3 (:k carry))
                         (reduced \K)
                         carry))))
                 {:k 0 :b 0}
                 (:converted pitch-map))))

(defn batless-obp
  "Given a sequence of `pitch-maps` calculate the on-base percentage."
  [pitch-maps]
  (let [at-bats (count pitch-maps)
        on-base (count (filter #(#{\B \H} (:outcome %)) pitch-maps))]
    (Float/parseFloat (format "%.3f" (double (/ on-base at-bats))))))

(defn game-maps->pitches
  "Convert game maps to a sequence of pitch strings."
  [game-maps]
  (->> game-maps
       (map #(map :pitches (:plays %)))
       (mapcat identity)))

(defn count-plays
  [game-maps]
  (->> game-maps
       (map :plays)
       flatten
       count))

(defn game-maps->obp
  "Given a list of game maps created by `game-seq->game-map` return the batless OBP."
  [game-maps]
  (->> game-maps
       game-maps->pitches
       (map remove-non-pitch-chars)

       (map (fn [pitch-str] {:original pitch-str}))

       (map add-batless-pitch-str)

       (map add-batless-outcome)

       batless-obp))

(defn retrosheet-dir->game-maps
  "Returns the game maps created by `game-seq->game-map` for the given Retrosheet
  files and `player-id`. The returned game maps are sorted by date."
  [retrosheet-dir player-id]
  (->> retrosheet-dir
       retrosheet-event-files
       ;; (map #(file->stats % player-id))
       (map #(file->gamemap % player-id))

       ;; combine each file sequence into one sequence of game maps.
       (mapcat identity)

       ;; sort by :date
       (sort-by :date)))

(defn retrosheet-dir->stats
  "Maps a directory to a sequence of pitch sequences for `player-id`."
  [retrosheet-dir player-id]
  (->> (retrosheet-dir->game-maps retrosheet-dir player-id)
       ;; count-plays
       game-maps->obp
       #_(take 10)))

(defn median
  "Customized version of https://rosettacode.org/wiki/Averages/Median#Clojure"
  [ns]
  (let [cnt (count ns)
        mid (bit-shift-right cnt 1)]
    (if (odd? cnt)
      (nth ns mid)
      [(nth ns (dec mid)) (nth ns mid)])))

(defn run-times
  "Calculate the batless OBP `n` times."
  [retrosheet-dir player-id n]
  (let [game-maps   (retrosheet-dir->game-maps retrosheet-dir player-id)
        obp-seq     (sort (repeatedly n #(game-maps->obp game-maps)))
        obp-seq-len (count obp-seq)]
    {;; :obp-seq obp-seq
     :max    (apply max obp-seq)
     :min    (apply min obp-seq)
     :median (median obp-seq)
     :mode   (->> obp-seq frequencies (sort-by second) reverse ffirst)}))

(defn filter-file
  "Returns a function for determining if a stats map is for the given `filename`."
  [filename]
  (fn [stats-map]
    (= filename (.getName (:file stats-map)))))

(defn plate-appearances-only
  "Given a sequence of stat maps return all the plate appearances as a single
  sequence."
  [stat-map-seq]
  (apply concat (map :pas stat-map-seq)))

;; (defn run
;;   [retrosheet-dir]
;;   ;; (apply concat (map :pas (stats-per-file retrosheet-dir))))
;;   (plate-appearances-only (stats-per-file retrosheet-dir)))
;;   ;; (plate-appearances-only (filter (filter-file "2004SFN.EVN") (stats-per-file retrosheet-dir))))
;;   ;; (stats-per-file retrosheet-dir))
;;   ;; (reduce + (stats-per-file retrosheet-dir)))

(comment
  (count (run "../2004eve"))

  (->> (run "../2004eve") (interpose \newline) (apply str) (spit "bb.txt"))

  (-> "../2004eve/2004BAL.EVA"
      (io/file)
      (file->stats "bondb001")
      :pas)

  (count (flatten (map :pas (retrosheet-dir->stats "../2004eve" "bondb001"))))

  #_())
